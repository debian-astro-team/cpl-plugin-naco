/* $Id: recipe_main.c,v 1.7 2009-05-27 11:48:34 llundin Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2009-05-27 11:48:34 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include <irplib_plugin.h>


/*----------------------------------------------------------------------------*/
/**
 * @defgroup recipe_main   General plugin tests
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
                            Function definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
  @brief    Find a plugin and submit it to some tests
  @return   0 iff succesful

 */
/*----------------------------------------------------------------------------*/
int main(void)
{

    const char * tags[] = {
        "ERROR_TAG",
        "IMG_STD_CATALOG",
        "SPC_MODEL",
        "CAL_FLAT_SPEC",
        "CAL_FLAT_SPEC",
        "CAL_FLAT_SPEC",
        "CAL_FLAT_SPEC",
        "CAL_FLAT_SPEC",
        "CAL_ARC_SPEC",
        "CAL_ARC_SPEC",
        "CAL_ARC_SPEC",
        "CAL_ARC_SPEC",
        "CAL_ARC_SPEC",
        "CAL_DETLIN_LAMP",
        "CAL_DETLIN_LAMP",
        "CAL_DETLIN_LAMP",
        "CAL_DETLIN_LAMP",
        "CAL_DETLIN_LAMP",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "IM_JITTER_SKY",
        "IM_JITTER_SKY",
        "IM_JITTER_SKY",
        "IM_JITTER_SKY",
        "IM_JITTER_SKY",
        "POL_JITTER_SKY",
        "POL_JITTER_SKY",
        "POL_JITTER_SKY",
        "POL_JITTER_SKY",
        "POL_JITTER_SKY",
        "SPEC_NODDING",
        "SPEC_NODDING",
        "SPEC_NODDING",
        "SPEC_NODDING",
        "SPEC_NODDING",
        "SPEC_JITTEROBJ",
        "SPEC_JITTEROBJ",
        "SPEC_JITTEROBJ",
        "SPEC_JITTEROBJ",
        "SPEC_JITTEROBJ",
        "TECH_FOCUS",
        "TECH_FOCUS",
        "TECH_FOCUS",
        "TECH_FOCUS",
        "TECH_FOCUS",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "CAL_DETLIN_DARK",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "IM_JITTER_OBJ",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "CAL_FLAT_LAMP",
        "SLIT_IMG",
        "SLIT_IMG",
        "SLIT_IMG",
        "SLIT_IMG",
        "SLIT_IMG",
        "CAL_PSF",
        "CAL_PSF",
        "CAL_PSF",
        "CAL_PSF",
        "CAL_PSF",
        "CAL_FLAT_TW",
        "CAL_FLAT_TW",
        "CAL_FLAT_TW",
        "CAL_FLAT_TW",
        "CAL_FLAT_TW",
        "CAL_STD_JITTER",
        "CAL_STD_JITTER",
        "CAL_STD_JITTER",
        "CAL_STD_JITTER",
        "CAL_STD_JITTER",
        "SP_NODDINGOBJ",
        "SP_NODDINGOBJ",
        "SP_NODDINGOBJ",
        "SP_NODDINGOBJ",
        "SP_NODDINGOBJ"
    };

    cpl_pluginlist * pluginlist;
    const size_t ntags = sizeof(tags) / sizeof(char*);

    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    pluginlist = cpl_pluginlist_new();

    cpl_test_zero(cpl_plugin_get_info(pluginlist));

    cpl_test_zero(irplib_plugin_test(pluginlist, ntags, tags));

    cpl_pluginlist_delete(pluginlist);

    return cpl_test_end(0);
}

/**@}*/
