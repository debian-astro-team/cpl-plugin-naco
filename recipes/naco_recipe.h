/* $Id: naco_recipe.h,v 1.20 2012-01-11 09:42:21 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2012-01-11 09:42:21 $
 * $Revision: 1.20 $
 * $Name: not supported by cvs2svn $
 */



/* include file for NACO recipes */
/* cpp macros that must be defined before inclusion:

   RECIPE_NAME:         e.g. naco_img_combine
   RECIPE_STRING:       e.g. "naco_img_combine"
   RECIPE_SYNOPSIS:     String with the recipe synopsis
   RECIPE_DESCRIPTION:  String with the recipe descrption
   Optionally:
   RECIPE_PARAMS:       Binary or of NACO parameter enums

*/


#ifndef NACO_RECIPE_H
#define NACO_RECIPE_H

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/* TEMPORARY SUPPORT OF CPL 5.x */
#ifndef CPL_SIZE_FORMAT
#define CPL_SIZE_FORMAT "d"
#define cpl_size int
#endif

#include "naco_utils.h"
#include "naco_pfits.h"
#include "naco_dfs.h"

#include "irplib_plugin.h"
#include "irplib_framelist.h"

#include "naco_parameter.h"

#include <math.h>
#include <assert.h>
#include <float.h>

/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

#define naco_pipe_id PACKAGE "/" PACKAGE_VERSION

#define NACO_RECIPE_DEFINE(RECIPE_NAME, RECIPE_PARAMS, SYNOPSIS, DESCRIPTION) \
    cpl_recipe_define(RECIPE_NAME, NACO_BINARY_VERSION,                 \
                      "Lars Lundin", PACKAGE_BUGREPORT,                 \
                      "2002, 2003, 2005, 2008",                         \
                      SYNOPSIS, DESCRIPTION);                           \
                                                                        \
    static cpl_error_code CPL_CONCAT2X(RECIPE_NAME,fill_parameterlist)  \
        (cpl_parameterlist * self) {                                    \
        return naco_parameter_set(self, #RECIPE_NAME, RECIPE_PARAMS)    \
            ? cpl_error_set_where(cpl_func) : CPL_ERROR_NONE;           \
    }                                                                   \
    extern int CPL_CONCAT2X(RECIPE_NAME,plugin_end)
   

/* FIXME: Copied from visir_error_reset() */
#define naco_error_reset(...)                                                  \
 irplib_error_recover(cleanstate, __VA_ARGS__)

#endif
