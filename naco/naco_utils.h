/* $Id: naco_utils.h,v 1.32 2008-04-15 17:04:21 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2008-04-15 17:04:21 $
 * $Revision: 1.32 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_UTILS_H
#define NACO_UTILS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "irplib_utils.h"
#include "irplib_framelist.h"

#include "irplib_plugin.h"

/*-----------------------------------------------------------------------------
                                   Function Prototypes
 -----------------------------------------------------------------------------*/

cpl_error_code naco_get_filter_infos(const char *, double *, double *);

const char ** naco_framelist_set_tag(irplib_framelist *,
                                     char * (*)(const cpl_frame *,
                                                const cpl_propertylist *, int),
                                     int *);

#endif
