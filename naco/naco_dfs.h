/* $Id: naco_dfs.h,v 1.46 2012-08-24 09:24:39 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2012-08-24 09:24:39 $
 * $Revision: 1.46 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_DFS_H
#define NACO_DFS_H

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

/* Define here the PRO.CATG keywords */
#define NACO_IMG_DARK_AVG           "NACO_IMG_DARK_AVG"
#define NACO_IMG_DARK_HOT           "NACO_IMG_DARK_HOT"
#define NACO_IMG_DARK_COLD          "NACO_IMG_DARK_COLD"
#define NACO_IMG_DARK_DEV           "NACO_IMG_DARK_DEV"

#define NACO_IMG_DETLIN_A           "NACO_IMG_DETLIN_A"
#define NACO_IMG_DETLIN_B           "NACO_IMG_DETLIN_B"
#define NACO_IMG_DETLIN_C           "NACO_IMG_DETLIN_C"
#define NACO_IMG_DETLIN_D           "NACO_IMG_DETLIN_D"
#define NACO_IMG_DETLIN_Q           "NACO_IMG_DETLIN_Q"
#define NACO_IMG_DETLIN_LIM         "NACO_IMG_DETLIN_LIM"
#define NACO_IMG_DETLIN_BPM         "NACO_IMG_DETLIN_BPM"

#define NACO_IMG_JITTER_COMB        "COADDED_IMG"
#define NACO_IMG_JITTER_CUBE        "COADDED_CUBE_IMG"

#define NACO_IMG_LAMPFLAT_RES       "MASTER_LAMP_FLAT"

#define NACO_IMG_TWFLAT_RES         "MASTER_IMG_FLAT"
#define NACO_IMG_TWFLAT_ERRMAP      "MASTER_IMG_FLAT_ERRMAP"
#define NACO_IMG_TWFLAT_INTER       "MASTER_IMG_FLAT_INTERC"
#define NACO_IMG_TWFLAT_BPM         "MASTER_IMG_FLAT_BADPIX"

#define NACO_IMG_SLITPOS_RES        "SLITPOS_TABLE"

#define NACO_IMG_ZPOINT_RES         "ZPOINT_TABLE"
#define NACO_IMG_ZPOINT_CHECK       "ZPOINT"

#define NACO_IMG_STREHL_RESCAL      "NACO_IMG_STREHL_CAL"
#define NACO_IMG_STREHL_RESTECH     "NACO_IMG_STREHL_TECH"

#define NACO_SPC_NOD_COMBINE        "SPC_NOD_COMBINED"
#define NACO_SPC_NOD_SUBTRACT       "SPC_NOD_SUBTRACTED"
#define NACO_SPC_JITTER_COMB        "SPC_COMBINED"
#define NACO_SPC_JITTER_EXTR        "SPC_EXTRACTED"

#define NACO_IMG_STD_CAT            "IMG_STD_CATALOG"
#define NACO_SPC_MODEL              "SPC_MODEL"
#define NACO_SPC_ARGON              "SPC_ARGON"

#define NACO_IMG_CHECKFOCUS         "CHECKFOCUS"

/* Define here the DO.CATG keywords */
#define NACO_CALIB_FLAT             "MASTER_IMG_FLAT"
#define NACO_CALIB_BPM              "MASTER_IMG_FLAT_BADPIX"
#define NACO_CALIB_DARK             "NACO_IMG_DARK_AVG"
#define NACO_CALIB_SPCFLAT          "MASTER_SPC_FLAT"
#define NACO_CALIB_ARC_DIFF         "ARC_IMAGE"
#define NACO_CALIB_ARC_MAP          "ARC_MAP"
#define NACO_CALIB_ARC              "ARC_COEF"
#define NACO_CALIB_ARC_WL           "ARC_COEF_WL"

#define NACO_IMG_DARK_RAW           "CAL_DARK"

#define NACO_IMG_DETLIN_LAMP        "CAL_DETLIN_LAMP"
#define NACO_IMG_DETLIN_DARK        "CAL_DETLIN_DARK"

#define NACO_IMG_JITTER_OBJ         "IM_JITTER_OBJ"
#define NACO_IMG_JITTER_SKY         "IM_JITTER_SKY"
#define NACO_IMG_JITTER_OBJ_POL     "POL_JITTER_OBJ"
#define NACO_IMG_JITTER_SKY_POL     "POL_JITTER_SKY"

#define NACO_IMG_LAMPFLAT_RAW       "CAL_FLAT_LAMP"

#define NACO_IMG_TWFLAT_RAW         "CAL_FLAT_TW"

/* TPL.ID NACO_spec_cal_LampFlats */
#define NACO_SPC_LAMPFLAT_RAW       "CAL_FLAT_SPEC"

/* TPL.ID NACO_spec_cal_Arcs */
#define NACO_SPC_LAMPWAVE_RAW       "CAL_ARC_SPEC"

/* TPL.ID NACO_spec_cal_StandardStar */
#define NACO_SPC_STD_RAW            "CAL_NOD_SPEC"

#define NACO_IMG_STREHL_CAL         "CAL_PSF"
#define NACO_IMG_STREHL_TECH        "TECH_PSF"

#define NACO_IMG_CHECKFOCUS_RAW     "TECH_FOCUS"

#define NACO_IMG_ZPOINT_JITTER      "CAL_STD_JITTER"
#define NACO_IMG_ZPOINT_CHOP        "CAL_STD_CHOP"

#define NACO_IMG_SLITPOS_RAW        "SLIT_IMG"

/* TPL.ID NACO_spec_obs_AutoNodOnSlit */
#define NACO_SPC_NOD_RAW            "SPEC_NODDING"

/* NACO_spec_obs_GenericOffset */
#define NACO_SPC_JITTER_RAW         "SPEC_JITTEROBJ"
#define NACO_SPC_JITTER_SKY         "SPEC_JITTERSKY"

#define NACO_IMG_STD_ASCII          "IMG_STD_ASCII"

#define NACO_SPC_MODEL_ASCII        "SPC_MODEL_ASCII"
#define NACO_SPC_ARGON_ASCII        "SPC_ARGON_ASCII"

/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

int naco_dfs_set_groups(cpl_frameset *);

#endif
