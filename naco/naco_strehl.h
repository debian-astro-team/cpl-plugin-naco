/* $Id: naco_strehl.h,v 1.46 2012-08-24 09:24:39 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2012-08-24 09:24:39 $
 * $Revision: 1.46 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_STREHL_H
#define NACO_STREHL_H

#include <cpl.h>

/*-----------------------------------------------------------------------------
                                Function prototypes
 -----------------------------------------------------------------------------*/

cpl_error_code naco_strehl_compute(const cpl_image         *,
                                   const cpl_parameterlist *,
                                   const char *,
                                   double, double, double, double, double,
                                   double *, double *, double *, double *,
                                   double *, double *, double *, double *);

#endif
