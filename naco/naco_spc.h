/* $Id: naco_spc.h,v 1.23 2008-09-12 09:17:51 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2008-09-12 09:17:51 $
 * $Revision: 1.23 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_SPC_H
#define NACO_SPC_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <irplib_framelist.h>

#include "naco_pfits.h"


/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

#define NACO_SPC_MODEL_COEFFS       4
/* The number of columns is 5 + NACO_SPC_MODEL_COEFFS */
#define NACO_SPC_MODEL_COLS         9

#define NACO_SPC_LAB_MODE           "SpecMode"
#define NACO_SPC_LAB_RESID          "Residual"
#define NACO_SPC_LAB_ORDER          "Fit_Order"
#define NACO_SPC_LAB_XMIN           "XMin"
#define NACO_SPC_LAB_XMAX           "XMax"
#define NACO_SPC_LAB_C1             "C_1"
#define NACO_SPC_LAB_C2             "C_2"
#define NACO_SPC_LAB_C3             "C_3"
#define NACO_SPC_LAB_C4             "C_4"

#define NACO_SPC_LAB_WAVE           "WAVELENGTH"
#define NACO_SPC_LAB_INTENS         "INTENSITY"

#define NACO_SPC_MAN_MODESPLIT                                          \
    "The input frames are split into groups with identical values of '" \
    NACO_PFITS_DOUBLE_DIT      "', '"                                   \
    NACO_PFITS_STRING_SLITNAME "', '"                                   \
    NACO_PFITS_DOUBLE_CWLEN    "' and '"                                \
    NACO_PFITS_STRING_SPECMODE "'."

/*-----------------------------------------------------------------------------
                                   Function Prototypes
 -----------------------------------------------------------------------------*/

char * naco_spc_make_tag(const cpl_frame*, const cpl_propertylist *, int);

cpl_error_code naco_imagelist_load_diff(cpl_imagelist *,
                                        const irplib_framelist *,
                                        const cpl_propertylist *);

cpl_error_code naco_imagelist_split(cpl_imagelist *);

cpl_error_code naco_imagelist_add_split(cpl_imagelist *);

cpl_error_code naco_imagelist_append_invert(cpl_imagelist *);

cpl_error_code naco_vector_correlate_imagelist_1d(cpl_vector *,
                                                  const cpl_vector *,
                                                  cpl_boolean,
                                                  const cpl_imagelist *);
#endif
