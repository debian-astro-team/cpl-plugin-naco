/* $Id: naco_dfs.c,v 1.22 2008-04-18 00:25:30 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2008-04-18 00:25:30 $
 * $Revision: 1.22 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <stdarg.h>
#include <string.h>
#include <math.h>

#include <cpl.h>

#include "naco_dfs.h"


/*----------------------------------------------------------------------------*/
/**
 * @defgroup naco_dfs  DFS related functions
 *
 * TBD
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Set the group as RAW or CALIB in a frameset
  @param    set     the input frameset
  @return   0 if ok, -1 in error case
 */
/*----------------------------------------------------------------------------*/
int naco_dfs_set_groups(cpl_frameset * set)
{
    cpl_frame   *   cur_frame;
    const char  *   tag;
    int             nframes;
    int             i;

    /* Check entries */
    if (set == NULL) return -1;
    
    /* Initialize */
    nframes = cpl_frameset_get_size(set);

    /* Loop on frames */
    for (i=0 ; i<nframes ; i++) {
        cur_frame = cpl_frameset_get_position(set, i);
        tag = cpl_frame_get_tag(cur_frame);
    
        /* RAW frames */
        if (!strcmp(tag, NACO_IMG_DARK_RAW) ||
                !strcmp(tag, NACO_IMG_DETLIN_LAMP) ||
                !strcmp(tag, NACO_IMG_DETLIN_DARK) ||
                !strcmp(tag, NACO_IMG_JITTER_OBJ) ||
                !strcmp(tag, NACO_IMG_JITTER_SKY) ||
                !strcmp(tag, NACO_IMG_JITTER_OBJ_POL) ||
                !strcmp(tag, NACO_IMG_JITTER_SKY_POL) ||
                !strcmp(tag, NACO_IMG_LAMPFLAT_RAW) ||
                !strcmp(tag, NACO_IMG_TWFLAT_RAW) ||
                !strcmp(tag, NACO_IMG_STREHL_CAL) ||
                !strcmp(tag, NACO_IMG_STREHL_TECH) ||
                !strcmp(tag, NACO_IMG_CHECKFOCUS_RAW) ||
                !strcmp(tag, NACO_IMG_SLITPOS_RAW) ||
                !strcmp(tag, NACO_IMG_ZPOINT_JITTER) ||
                !strcmp(tag, NACO_IMG_ZPOINT_CHOP) ||
                !strcmp(tag, NACO_IMG_STD_ASCII) ||
                !strcmp(tag, NACO_SPC_MODEL_ASCII) ||
                !strcmp(tag, NACO_SPC_ARGON_ASCII) ||
                !strcmp(tag, NACO_SPC_LAMPWAVE_RAW) ||
                !strcmp(tag, NACO_SPC_LAMPFLAT_RAW) ||
                !strcmp(tag, NACO_SPC_NOD_RAW) ||
                !strcmp(tag, NACO_SPC_JITTER_RAW) ||
                !strcmp(tag, NACO_SPC_JITTER_SKY))
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_RAW);
        /* CALIB frames */
        else if (!strcmp(tag, NACO_CALIB_FLAT) ||
                !strcmp(tag, NACO_CALIB_BPM) ||
                !strcmp(tag, NACO_CALIB_SPCFLAT) ||
                !strcmp(tag, NACO_CALIB_ARC) ||
                !strcmp(tag, NACO_IMG_STD_CAT) ||
                !strcmp(tag, NACO_SPC_MODEL) ||
                !strcmp(tag, NACO_SPC_ARGON) ||
                !strcmp(tag, NACO_CALIB_ARC_WL))
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_CALIB);

    }
    return 0;
}

/**@}*/
