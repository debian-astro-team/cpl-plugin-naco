/* $Id: irplib_pfits.h,v 1.12 2008-04-11 22:46:00 llundin Exp $
 *
 * This file is part of the irplib package 
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2008-04-11 22:46:00 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifndef IRPLIB_PFITS_H
#define IRPLIB_PFITS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "irplib_utils.h"
#include "irplib_framelist.h"

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

/* Maximum size of FITS string value */
#define IRPLIB_FITS_STRLEN 68

#define irplib_pfits_get_bool(LIST, KEY) \
  irplib_pfits_get_bool_macro(LIST, KEY, cpl_func, __FILE__, __LINE__)

#define irplib_pfits_get_double(LIST, KEY) \
  irplib_pfits_get_double_macro(LIST, KEY, cpl_func, __FILE__, __LINE__)

#define irplib_pfits_get_int(LIST, KEY) \
  irplib_pfits_get_int_macro(LIST, KEY, cpl_func, __FILE__, __LINE__)

#define irplib_pfits_get_string(LIST, KEY) \
  irplib_pfits_get_string_macro(LIST, KEY, cpl_func, __FILE__, __LINE__)

#define IRPLIB_PFITS_STRING_DPR_CATG          "ESO DPR CATG"
#define IRPLIB_PFITS_STRING_DPR_TECH          "ESO DPR TECH"
#define IRPLIB_PFITS_STRING_DPR_TYPE          "ESO DPR TYPE"

#define IRPLIB_PFITS_REGEXP_DPR      \
    IRPLIB_PFITS_STRING_DPR_CATG "|" \
    IRPLIB_PFITS_STRING_DPR_TECH "|" \
    IRPLIB_PFITS_STRING_DPR_TYPE

#define IRPLIB_PFITS_WCS_REGEXP "WCSAXES|WCSNAME|(PC|CD|PV|PS)[0-9]+_[0-9]+|" \
                                "C(RVAL|RPIX|DELT|TYPE|UNIT|RDER|SYER)[0-9]+"

#define IRPLIB_PFITS_REGEXP_PAF "DATE-OBS|MJD-OBS|ARCFILE|ESO TPL ID"

/* DATE + EXTEND are already done, WCS needs handling elsewhere */
#define IRPLIB_PFITS_REGEXP_RECAL_LAMP "EXPTIME"
/* DATE + EXTEND are already done, WCS needs handling elsewhere */
#define IRPLIB_PFITS_REGEXP_RECAL IRPLIB_PFITS_REGEXP_RECAL_LAMP "|AIRMASS"

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

#define IRPLIB_DFS_DOCATG_FROM_DPR(DOCATG, MATCH_CATG,                         \
                                           MATCH_TYPE,                         \
                                           MATCH_TECH)                         \
    if ((docatg == NULL) && ((irplib_dfs_find_words MATCH_CATG) == 0)          \
                         && ((irplib_dfs_find_words MATCH_TYPE) == 0)          \
                         && ((irplib_dfs_find_words MATCH_TECH) == 0))         \
        docatg = (DOCATG);

/*-----------------------------------------------------------------------------
                            Function prototypes
  -----------------------------------------------------------------------------*/

const char * irplib_pfits_get_dpr_catg(const cpl_propertylist *);
const char * irplib_pfits_get_dpr_tech(const cpl_propertylist *);
const char * irplib_pfits_get_dpr_type(const cpl_propertylist *);

cpl_boolean irplib_pfits_get_bool_macro(const cpl_propertylist * ,
					const char *, const char *,
					const char *, unsigned);

double irplib_pfits_get_double_macro(const cpl_propertylist *,
                                     const char *, const char *,
                                     const char *, unsigned);

int irplib_pfits_get_int_macro(const cpl_propertylist * ,
                               const char *, const char *,
                               const char *, unsigned);

const char * irplib_pfits_get_string_macro(const cpl_propertylist * ,
                                           const char *, const char *,
                                           const char *, unsigned);

/* Other functions */

cpl_error_code irplib_pfits_set_airmass(cpl_propertylist *,
                                        const irplib_framelist *);

int irplib_dfs_find_words(const char *, const char *, ...)
#ifdef __GNUC__
    __attribute__((format (printf, 2, 3)))
#endif
;

cpl_error_code irplib_dfs_check_framelist_tag(const irplib_framelist *,
                                              const char * (*)(const char*,
                                                               const char*,
                                                               const char*));
#endif
