/* $Id: naco_pfits.c,v 1.14 2008-11-05 09:59:59 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2008-11-05 09:59:59 $
 * $Revision: 1.14 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <string.h>
#include <cpl.h>

#include "naco_pfits.h"

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

#define naco_pfits_get_bool(KEY)   irplib_pfits_get_bool(self, KEY)
#define naco_pfits_get_double(KEY) irplib_pfits_get_double(self, KEY)
#define naco_pfits_get_int(KEY)    irplib_pfits_get_int(self, KEY)
#define naco_pfits_get_string(KEY) irplib_pfits_get_string(self, KEY)


/*----------------------------------------------------------------------------*/
/**
 * @defgroup naco_pfits     FITS header protected access
 *
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
                              Function codes
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    find out airmass start
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_airmass_start(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO TEL AIRM START");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out airmass end
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_airmass_end(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO TEL AIRM END");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in X
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_cumoffsetx(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO SEQ CUMOFFSETX");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the cumulative offset in Y
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_cumoffsety(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO SEQ CUMOFFSETY");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DEC
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_dec(const cpl_propertylist * self)
{
    return naco_pfits_get_double("DEC");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the DIT
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_dit(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO DET DIT");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the ECMEAN key
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_ecmean(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS RTC DET DST ECMEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the exposure time
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_exptime(const cpl_propertylist * self)
{
    return naco_pfits_get_double("EXPTIME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the filter
  @param    self       property list to read from
  @return   pointer to statically allocated character string or NULL
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_filter(const cpl_propertylist * self)
{
    const char    *    val;
    cpl_errorstate prestate = cpl_errorstate_get();

    /* Get value for OPTI5.ID */
    val = naco_pfits_get_string("ESO INS OPTI5 ID");

    if (val == NULL) {
        irplib_error_recover(prestate, "Could not get FITS key:");
    } else if (strcmp(val, "empty")) {
        /* OPTI5.ID is non-empty */
        return val;
    }

    /* Get value for OPTI6.ID */
    val = naco_pfits_get_string("ESO INS OPTI6 ID");

    if (val == NULL) {
        irplib_error_recover(prestate, "Could not get FITS key:");
    } else if (strcmp(val, "empty")) {
        /* OPTI6.ID is non-empty */
        return val;
    }

    /* Get value for OPTI4.ID */
    val = naco_pfits_get_string("ESO INS OPTI4 ID");
    cpl_ensure(val != NULL, cpl_error_get_code(), NULL);
    cpl_ensure(strcmp(val, "empty"), CPL_ERROR_ILLEGAL_OUTPUT, NULL);

    /* OPTI4.ID is non-empty */
    return val;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the FLUXMEAN key
  @param    self       property list to read from
  @return      the requested value 
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_fluxmean(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS RTC DET DST FLUXMEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the focus
  @param    self       property list to read from
  @return      the requested value 
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_focus(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS INS FOCU ABSPOS");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the humidity level
  @param    self       property list to read from
  @return      the requested value 
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_humidity_level(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO TEL AMBI RHUM");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the L0MEAN
  @param    self       property list to read from
  @return      the requested value 
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_l0mean(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS RTC DET DST L0MEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the INS.LAMP2.SET keyword
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int naco_pfits_get_lamp2(const cpl_propertylist * self)
{
    return naco_pfits_get_int("ESO INS LAMP2 SET");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the mode name
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_mode(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO DET MODE NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Get the Object name 
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_object(const cpl_propertylist * self)
{
    return naco_pfits_get_string(NACO_PFITS_STRING_OBJECT);
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the NDIT keyword
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int naco_pfits_get_ndit(const cpl_propertylist * self)
{
    return naco_pfits_get_int("ESO DET NDIT");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OPTI3.NAME key
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_opti3_name(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO INS OPTI3 NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OPTI4.NAME key
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_opti4_name(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO INS OPTI4 NAME");
}


/*----------------------------------------------------------------------------*/
/**
  @brief    find out the OPTI7.NAME key
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_opti7_name(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO INS OPTI7 NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the pixel scale
  @param    self       property list to read from
  @return      the requested value 
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_pixscale(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO INS PIXSCALE");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the R0MEAN key
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_r0mean(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS RTC DET DST R0MEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the RA
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_ra(const cpl_propertylist * self)
{
    return naco_pfits_get_double("RA");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the read out mode
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
int naco_pfits_get_rom(const cpl_propertylist * self)
{
    return naco_pfits_get_int("ESO DET NCORRS");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the read out mode name
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_rom_name(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO DET NCORRS NAME");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the T0MEAN key
  @param    self       property list to read from
  @return   the requested value
 */
/*----------------------------------------------------------------------------*/
double naco_pfits_get_t0mean(const cpl_propertylist * self)
{
    return naco_pfits_get_double("ESO AOS RTC DET DST T0MEAN");
}

/*----------------------------------------------------------------------------*/
/**
  @brief    find out the template ID
  @param    self       property list to read from
  @return   pointer to statically allocated character string
 */
/*----------------------------------------------------------------------------*/
const char * naco_pfits_get_templateid(const cpl_propertylist * self)
{
    return naco_pfits_get_string("ESO TPL ID");
}

/**@}*/
