/* $Id: naco_pfits.h,v 1.39 2009-11-15 21:40:34 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2009-11-15 21:40:34 $
 * $Revision: 1.39 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_PFITS_H
#define NACO_PFITS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "irplib_pfits.h"

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

#define NACO_PFITS_DOUBLE_DEC                "DEC"
#define NACO_PFITS_DOUBLE_FOCUS              "ESO AOS INS FOCU ABSPOS"
#define NACO_PFITS_DOUBLE_ECMEAN             "ESO AOS RTC DET DST ECMEAN"
#define NACO_PFITS_DOUBLE_FLUXMEAN           "ESO AOS RTC DET DST FLUXMEAN"
#define NACO_PFITS_DOUBLE_L0MEAN             "ESO AOS RTC DET DST L0MEAN"
#define NACO_PFITS_DOUBLE_R0MEAN             "ESO AOS RTC DET DST R0MEAN"
#define NACO_PFITS_DOUBLE_T0MEAN             "ESO AOS RTC DET DST T0MEAN"
#define NACO_PFITS_DOUBLE_DIT                "ESO DET DIT"
#define NACO_PFITS_STRING_MODE               "ESO DET MODE NAME"
#define NACO_PFITS_INT_ROM                   "ESO DET NCORRS"
#define NACO_PFITS_STRING_ROM_NAME           "ESO DET NCORRS NAME"
#define NACO_PFITS_INT_NDIT                  "ESO DET NDIT"
#define NACO_PFITS_BOOL_LAMP1                "ESO INS LAMP1 ST"
#define NACO_PFITS_STRING_SPECMODE           "ESO SEQ SPECMODE"
#define NACO_PFITS_DOUBLE_CWLEN              "ESO INS CWLEN"
#define NACO_PFITS_STRING_SLITNAME           "ESO INS OPTI1 ID"
#define NACO_PFITS_INT_LAMP2                 "ESO INS LAMP2 SET"
#define NACO_PFITS_STRING_OPTI3_NAME         "ESO INS OPTI3 NAME"
#define NACO_PFITS_STRING_OPTI4_NAME         "ESO INS OPTI4 NAME"
#define NACO_PFITS_STRING_OPTI7_NAME         "ESO INS OPTI7 NAME"
#define NACO_PFITS_DOUBLE_PIXSCALE           "ESO INS PIXSCALE"
#define NACO_PFITS_DOUBLE_CUMOFFSETX         "ESO SEQ CUMOFFSETX"
#define NACO_PFITS_DOUBLE_CUMOFFSETY         "ESO SEQ CUMOFFSETY"
#define NACO_PFITS_DOUBLE_AIRMASS_END        "ESO TEL AIRM END"
#define NACO_PFITS_DOUBLE_AIRMASS_START      "ESO TEL AIRM START"
#define NACO_PFITS_DOUBLE_HUMIDITY_LEVEL     "ESO TEL AMBI RHUM"
#define NACO_PFITS_STRING_TEMPLATEID         "ESO TPL ID"
#define NACO_PFITS_DOUBLE_EXPTIME            "EXPTIME"
#define NACO_PFITS_STRING_OBJECT             "OBJECT"
#define NACO_PFITS_DOUBLE_RA                 "RA"

#define NACO_PFITS_STRING_OPTI4              "ESO INS OPTI4 ID"
#define NACO_PFITS_STRING_OPTI5              "ESO INS OPTI5 ID"
#define NACO_PFITS_STRING_OPTI6              "ESO INS OPTI6 ID"


/* Properties used by naco_img_zpoint */
#define NACO_PFITS_REGEXP_ZPOINT_PAF            \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    NACO_PFITS_DOUBLE_AIRMASS_START "|"         \
    NACO_PFITS_STRING_ROM_NAME "|"              \
    NACO_PFITS_STRING_MODE "|"                  \
    NACO_PFITS_DOUBLE_DIT  "|"                  \
    NACO_PFITS_DOUBLE_PIXSCALE "|"              \
    NACO_PFITS_DOUBLE_RA "|"                    \
    NACO_PFITS_DOUBLE_DEC "|"                   \
    "ESO INS OPTI7 ID|"                         \
    "ESO OBS ID|ESO AOS INS DICH POSNAM|"       \
    "ESO AOS OCS WFS MODE|ESO AOS OCS WFS TYPE"

#define NACO_PFITS_REGEXP_ZPOINT_REF            \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_STRING_OPTI3_NAME "|"            \
    NACO_PFITS_STRING_OPTI4_NAME "|"            \
    NACO_PFITS_STRING_OBJECT "|"                \
    NACO_PFITS_DOUBLE_PIXSCALE

#define NACO_PFITS_REGEXP_ZPOINT                \
    NACO_PFITS_DOUBLE_FLUXMEAN "|"              \
    NACO_PFITS_DOUBLE_L0MEAN "|"                \
    NACO_PFITS_DOUBLE_R0MEAN "|"                \
    NACO_PFITS_DOUBLE_T0MEAN "|"                \
    NACO_PFITS_DOUBLE_HUMIDITY_LEVEL "|"        \
    NACO_PFITS_DOUBLE_ECMEAN "|"                \
    NACO_PFITS_DOUBLE_AIRMASS_START "|"         \
    NACO_PFITS_DOUBLE_AIRMASS_END "|"           \
    NACO_PFITS_DOUBLE_CUMOFFSETX "|"            \
    NACO_PFITS_DOUBLE_CUMOFFSETY

/* Properties used by naco_img_slitpos */

#define NACO_PFITS_REGEXP_SLITPOS_PAF           \
    IRPLIB_PFITS_REGEXP_PAF    "|"              \
    IRPLIB_PFITS_REGEXP_DPR    "|"              \
    NACO_PFITS_STRING_SLITNAME "|"              \
    "INSTRUME|ESO TPL NEXP|ESO INS OPTI3 ID|"   \
    "ESO INS OPTI7 ID|ESO ADA ABSROT START"

#define NACO_PFITS_REGEXP_SLITPOS    \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6

/* Properties used by naco_img_strehl */

#define NACO_PFITS_REGEXP_STREHL_PAF            \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    NACO_PFITS_DOUBLE_DIT  "|"                  \
    NACO_PFITS_INT_NDIT "|"                     \
    NACO_PFITS_STRING_OPTI7_NAME "|"            \
    NACO_PFITS_STRING_ROM_NAME "|"              \
    NACO_PFITS_STRING_MODE "|"                  \
    NACO_PFITS_DOUBLE_PIXSCALE "|"              \
    "ESO OBS ID|ESO AOS INS DICH POSNAM|"       \
    "ESO AOS OCS WFS MODE|ESO AOS OCS WFS TYPE"

/* OK: NACO_PFITS_INT_NDIT is present also in _PAF */
#define NACO_PFITS_REGEXP_STREHL                \
    NACO_PFITS_DOUBLE_AIRMASS_END "|"           \
    NACO_PFITS_DOUBLE_AIRMASS_START "|"         \
    NACO_PFITS_DOUBLE_ECMEAN "|"                \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_DOUBLE_FLUXMEAN "|"              \
    NACO_PFITS_DOUBLE_L0MEAN "|"                \
    NACO_PFITS_DOUBLE_R0MEAN "|"                \
    NACO_PFITS_DOUBLE_T0MEAN "|"                \
    NACO_PFITS_INT_NDIT "|"                     \
    NACO_PFITS_STRING_OPTI3_NAME "|"            \
    NACO_PFITS_STRING_OPTI4_NAME "|"            \
    NACO_PFITS_DOUBLE_PIXSCALE

/* Properties used by naco_img_twflat */

#define NACO_PFITS_REGEXP_TWFLAT_DARK           \
    NACO_PFITS_DOUBLE_DIT "|"                   \
    NACO_PFITS_STRING_MODE "|"                  \
    NACO_PFITS_STRING_ROM_NAME

#define NACO_PFITS_REGEXP_TWFLAT                \
    NACO_PFITS_REGEXP_TWFLAT_DARK "|"           \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_STRING_TEMPLATEID

/* Properties used by naco_img_checkfocus */

#define NACO_PFITS_REGEXP_CHECKFOCUS_PAF        \
        IRPLIB_PFITS_REGEXP_PAF

#define NACO_PFITS_REGEXP_CHECKFOCUS            \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_DOUBLE_PIXSCALE "|"              \
    NACO_PFITS_DOUBLE_FOCUS

/* Properties used by naco_img_dark */

#define NACO_PFITS_REGEXP_DARK_PAF              \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    IRPLIB_PFITS_STRING_DPR_TECH "|"            \
    NACO_PFITS_DOUBLE_DIT "|"                   \
    NACO_PFITS_INT_NDIT "|"                     \
    NACO_PFITS_INT_ROM "|"                      \
    NACO_PFITS_STRING_MODE "|"                  \
    NACO_PFITS_STRING_ROM_NAME

/* All recipes may use IRPLIB_PFITS_REGEXP_DPR */
#define NACO_PFITS_REGEXP_DARK                  \
    NACO_PFITS_DOUBLE_EXPTIME "|"               \
    NACO_PFITS_STRING_OPTI7_NAME "|"            \
    "NAXIS[12]"


/* Properties used by naco_spc_combine */

#define NACO_PFITS_REGEXP_SPC_COMBINE_PAF       \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    "NAXIS1|NAXIS2|"                            \
    NACO_PFITS_DOUBLE_DIT "|"                   \
    NACO_PFITS_STRING_OPTI4  "|"                \
    NACO_PFITS_STRING_OPTI5  "|"                \
    NACO_PFITS_STRING_OPTI6  "|"                \
    NACO_PFITS_REGEXP_JITTER_ALL

#define NACO_PFITS_REGEXP_SPC_COMBINE           \
    "NAXIS1|NAXIS2|"                            \
    NACO_PFITS_DOUBLE_DIT "|"                   \
    NACO_PFITS_REGEXP_JITTER_ALL

/* Properties used by naco_spc_wavecal */

/* Properties used by naco_spc_lampflat */
#define NACO_PFITS_REGEXP_SPCWAVE_PAF           \
    IRPLIB_PFITS_REGEXP_PAF    "|"              \
    IRPLIB_PFITS_REGEXP_DPR    "|"              \
    NACO_PFITS_STRING_SPECMODE "|"              \
    NACO_PFITS_STRING_SLITNAME "|"              \
    NACO_PFITS_DOUBLE_CWLEN    "|"              \
    NACO_PFITS_DOUBLE_DIT      "|"              \
    "INSTRUME|ESO TPL NEXP|"                    \
    "ESO INS GRAT NAME|"                        \
    "ESO INS MODE"


#define NACO_PFITS_REGEXP_SPCWAVE               \
    IRPLIB_PFITS_REGEXP_RECAL "|"               \
    "NAXIS1|NAXIS2|"                            \
    NACO_PFITS_INT_LAMP2 "|"                    \
    NACO_PFITS_DOUBLE_PIXSCALE "|"              \
    NACO_PFITS_BOOL_LAMP1

/* Properties used by naco_img_lampflat */

#define NACO_PFITS_REGEXP_LAMPFLAT_PAF          \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    NACO_PFITS_DOUBLE_DIT   "|"                 \
    NACO_PFITS_INT_LAMP2    "|"                 \
    NACO_PFITS_INT_NDIT     "|"                 \
    NACO_PFITS_INT_ROM      "|"                 \
    NACO_PFITS_STRING_MODE  "|"                 \
    NACO_PFITS_STRING_ROM_NAME "|"              \
    NACO_PFITS_STRING_OPTI7_NAME "|"            \
    "ESO INS LAMP2 NAME|ESO INS LAMP2 TYPE|ESO INS LAMP2 CURRENT"


/* All recipes may use IRPLIB_PFITS_REGEXP_DPR */
#define NACO_PFITS_REGEXP_LAMPFLAT              \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_STRING_OPTI3_NAME "|"            \
    NACO_PFITS_STRING_OPTI4_NAME

/* Properties used by naco_spc_lampflat */
#define NACO_PFITS_REGEXP_SPCFLAT_PAF           \
    IRPLIB_PFITS_REGEXP_PAF    "|"              \
    NACO_PFITS_STRING_SPECMODE "|"              \
    NACO_PFITS_STRING_SLITNAME "|"              \
    NACO_PFITS_DOUBLE_CWLEN    "|"              \
    NACO_PFITS_DOUBLE_DIT

#define NACO_PFITS_REGEXP_SPCFLAT               \
    IRPLIB_PFITS_REGEXP_RECAL  "|"              \
    "NAXIS1|NAXIS2|"                            \
    NACO_PFITS_BOOL_LAMP1   "|"                 \
    NACO_PFITS_INT_LAMP2

/* Properties used by naco_img_jitter */
#define NACO_PFITS_REGEXP_JITTER_PAF            \
    IRPLIB_PFITS_REGEXP_PAF "|"                 \
    IRPLIB_PFITS_REGEXP_DPR "|"                 \
    NACO_PFITS_DOUBLE_PIXSCALE "|"              \
    "INSTRUME|ESO TPL NEXP|ESO OBS ID"

#define NACO_PFITS_REGEXP_JITTER_COPY           \
        IRPLIB_PFITS_REGEXP_RECAL "|"           \
        "ALARM"

/* - these are needed for all frames (recalc airmass) */
#define NACO_PFITS_REGEXP_JITTER_ALL            \
    NACO_PFITS_DOUBLE_CUMOFFSETX "|"            \
    NACO_PFITS_DOUBLE_CUMOFFSETY "|"            \
    NACO_PFITS_STRING_OPTI4      "|"            \
    NACO_PFITS_STRING_OPTI5      "|"            \
    NACO_PFITS_STRING_OPTI6      "|"            \
    NACO_PFITS_DOUBLE_PIXSCALE   "|"            \
    "NAXIS3"                     "|"            \
    "AIRMASS"

#define NACO_PFITS_REGEXP_JITTER_FIRST          \
    NACO_PFITS_REGEXP_JITTER_ALL "|"            \
    NACO_PFITS_REGEXP_JITTER_PAF "|"            \
    NACO_PFITS_REGEXP_JITTER_COPY "|"           \
    IRPLIB_PFITS_WCS_REGEXP       "|"           \
    NACO_PFITS_STRING_OPTI4 "|"                 \
    NACO_PFITS_STRING_OPTI5 "|"                 \
    NACO_PFITS_STRING_OPTI6 "|"                 \
    NACO_PFITS_STRING_OPTI3_NAME "|"            \
    NACO_PFITS_STRING_OPTI4_NAME

#define NACO_PFITS_REGEXP_DETLIN                \
    NACO_PFITS_DOUBLE_DIT

#define NACO_PFITS_REGEXP_DETLIN_COPY           \
    NACO_PFITS_DOUBLE_EXPTIME

#define NACO_PFITS_REGEXP_DETLIN_FIRST          \
    NACO_PFITS_REGEXP_DETLIN "|"                \
    NACO_PFITS_REGEXP_DETLIN_COPY

/*-----------------------------------------------------------------------------
                                   Function prototypes
 -----------------------------------------------------------------------------*/

double naco_pfits_get_airmass_start(const cpl_propertylist *);
double naco_pfits_get_airmass_end(const cpl_propertylist *);
double naco_pfits_get_cumoffsetx(const cpl_propertylist *);
double naco_pfits_get_cumoffsety(const cpl_propertylist *);
double naco_pfits_get_dec(const cpl_propertylist *);
double naco_pfits_get_dit(const cpl_propertylist *);
double naco_pfits_get_ecmean(const cpl_propertylist *);
double naco_pfits_get_exptime(const cpl_propertylist *);
const char * naco_pfits_get_filter(const cpl_propertylist *);
double naco_pfits_get_fluxmean(const cpl_propertylist *);
double naco_pfits_get_focus(const cpl_propertylist *);
double naco_pfits_get_humidity_level(const cpl_propertylist *);
double naco_pfits_get_l0mean(const cpl_propertylist *);
int naco_pfits_get_lamp2(const cpl_propertylist *);
const char * naco_pfits_get_mode(const cpl_propertylist *);
int naco_pfits_get_ndit(const cpl_propertylist *);
const char * naco_pfits_get_object(const cpl_propertylist *);
const char * naco_pfits_get_opti3_name(const cpl_propertylist *);
const char * naco_pfits_get_opti4_name(const cpl_propertylist *);
const char * naco_pfits_get_opti7_name(const cpl_propertylist *);
double naco_pfits_get_pixscale(const cpl_propertylist *);
double naco_pfits_get_r0mean(const cpl_propertylist *);
double naco_pfits_get_ra(const cpl_propertylist *);
int naco_pfits_get_rom(const cpl_propertylist *);
const char * naco_pfits_get_rom_name(const cpl_propertylist *);
double naco_pfits_get_t0mean(const cpl_propertylist *);
const char * naco_pfits_get_templateid(const cpl_propertylist *);

#endif 
