/* $Id: naco_physicalmodel.c,v 1.8 2006-11-15 08:25:03 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2006-11-15 08:25:03 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>
#include <math.h>
#include <float.h>

#include "naco_utils.h"
#include "naco_physicalmodel.h"
#include "naco_pfits.h"

/*----------------------------------------------------------------------------*/
/**
 * @defgroup naco_physicalmodel     Physical model of NACO
 */
/*----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                                Function codes
 -----------------------------------------------------------------------------*/

/**@{*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Estimate the instrument wavelength range.
  @param    filename    input file name
  @param    poly_deg    polynomial degree
  @return   1 newly allocated array of poly_deg+1 polynomical coefficients
  
  From a physical model of the instrument, find out the wavelength range
  associated to a given instrument configuration. The returned coefficients
  are such as wave = c[0] + c[1] * pix + ... + c[poly_deg] * pix^poly_deg 

  Instrument configuration informations are fetched from the FITS header.
 */
/*----------------------------------------------------------------------------*/
double * naco_get_disprel_estimate(
        const char  *   filename, 
        int             poly_deg)
{
    double              *   coefs;


    cpl_ensure(filename != NULL, CPL_ERROR_NULL_INPUT,    NULL);
    cpl_ensure(poly_deg >= 1,    CPL_ERROR_ILLEGAL_INPUT, NULL);

    coefs = cpl_calloc((size_t)poly_deg+1, sizeof(double));
    coefs[1] = 1.0;
    
    return coefs;
}

/**@}*/

