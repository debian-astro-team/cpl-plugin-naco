/* $Id: naco_parameter.h,v 1.15 2011-11-18 09:05:51 llundin Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: llundin $
 * $Date: 2011-11-18 09:05:51 $
 * $Revision: 1.15 $
 * $Name: not supported by cvs2svn $
 */

#ifndef NACO_PARAMETER_H
#define NACO_PARAMETER_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include "naco_utils.h"

/*-----------------------------------------------------------------------------
                                   Defines
 -----------------------------------------------------------------------------*/

/* The available recipe parameters */
/* None has the value 1, which makes the (mis)use of logical or detectable */
#define NACO_PARAM_PLOT     ((naco_parameter) 1 <<  1)
#define NACO_PARAM_STAR_R   ((naco_parameter) 1 <<  2)
#define NACO_PARAM_BG_RINT  ((naco_parameter) 1 <<  3)
#define NACO_PARAM_BG_REXT  ((naco_parameter) 1 <<  4)
#define NACO_PARAM_REJBORD  ((naco_parameter) 1 <<  5)
#define NACO_PARAM_HOT_LIM  ((naco_parameter) 1 <<  6)
#define NACO_PARAM_COLD_LIM ((naco_parameter) 1 <<  7)
#define NACO_PARAM_DEV_LIM  ((naco_parameter) 1 <<  8)
#define NACO_PARAM_NSAMPLES ((naco_parameter) 1 <<  9)
#define NACO_PARAM_HALFSIZE ((naco_parameter) 1 << 10)
#define NACO_PARAM_FORCE    ((naco_parameter) 1 << 11)
#define NACO_PARAM_SLIT_W   ((naco_parameter) 1 << 12)
#define NACO_PARAM_BPMTHRES ((naco_parameter) 1 << 13)
#define NACO_PARAM_PROPFIT  ((naco_parameter) 1 << 14)
#define NACO_PARAM_BPM      ((naco_parameter) 1 << 15)
#define NACO_PARAM_ERRORMAP ((naco_parameter) 1 << 16)
#define NACO_PARAM_INTCEPT  ((naco_parameter) 1 << 17)
#define NACO_PARAM_RA       ((naco_parameter) 1 << 18)
#define NACO_PARAM_DEC      ((naco_parameter) 1 << 19)
#define NACO_PARAM_PIXSCALE ((naco_parameter) 1 << 20)
#define NACO_PARAM_MAGNITD  ((naco_parameter) 1 << 21)
#define NACO_PARAM_SX       ((naco_parameter) 1 << 22)
#define NACO_PARAM_SY       ((naco_parameter) 1 << 23)
#define NACO_PARAM_CHK_IMG  ((naco_parameter) 1 << 24)
#define NACO_PARAM_SAVE     ((naco_parameter) 1 << 25)
#define NACO_PARAM_XTMIN    ((naco_parameter) 1 << 26)
#define NACO_PARAM_XTMAX    ((naco_parameter) 1 << 27)
#define NACO_PARAM_OFFSETS  ((naco_parameter) 1 << 28)
#define NACO_PARAM_OBJECTS  ((naco_parameter) 1 << 29)
#define NACO_PARAM_ODDEVEN  ((naco_parameter) 1 << 30)
#define NACO_PARAM_XCORR    ((naco_parameter) 1 << 31)
#define NACO_PARAM_UNION    ((naco_parameter) 1 << 32)
#define NACO_PARAM_REJ_HILO ((naco_parameter) 1 << 33)
#define NACO_PARAM_COMBINE  ((naco_parameter) 1 << 34)
#define NACO_PARAM_SKYPLANE ((naco_parameter) 1 << 35)
#define NACO_PARAM_CUBEMODE ((naco_parameter) 1 << 36)
#define NACO_PARAM_LUCK_STR ((naco_parameter) 1 << 37)
#define NACO_PARAM_SAVECUBE ((naco_parameter) 1 << 38)

/*-----------------------------------------------------------------------------
                                       Prototypes
 -----------------------------------------------------------------------------*/

typedef unsigned long long naco_parameter;

cpl_error_code naco_parameter_set(cpl_parameterlist *, const char *,
                                  naco_parameter);

cpl_boolean naco_parameterlist_get_bool(const cpl_parameterlist *,
                                        const char *,
                                        naco_parameter);

int naco_parameterlist_get_int(const cpl_parameterlist *,
                               const char *,
                               naco_parameter);

double naco_parameterlist_get_double(const cpl_parameterlist *,
                                     const char *,
                                     naco_parameter);

const char * naco_parameterlist_get_string(const cpl_parameterlist *,
                                           const char *,
                                           naco_parameter);

#endif
