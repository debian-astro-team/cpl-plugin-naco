
# NACO_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([NACO_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# NACO_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([NACO_SET_VERSION_INFO],
[
    naco_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    naco_major_version=`echo "$naco_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    naco_minor_version=`echo "$naco_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    naco_micro_version=`echo "$naco_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$naco_major_version"; then naco_major_version=0
    fi

    if test -z "$naco_minor_version"; then naco_minor_version=0
    fi

    if test -z "$naco_micro_version"; then naco_micro_version=0
    fi

    NACO_VERSION="$naco_version"
    NACO_MAJOR_VERSION=$naco_major_version
    NACO_MINOR_VERSION=$naco_minor_version
    NACO_MICRO_VERSION=$naco_micro_version

    if test -z "$4"; then NACO_INTERFACE_AGE=0
    else NACO_INTERFACE_AGE="$4"
    fi

    NACO_BINARY_AGE=`expr 100 '*' $NACO_MINOR_VERSION + $NACO_MICRO_VERSION`
    NACO_BINARY_VERSION=`expr 10000 '*' $NACO_MAJOR_VERSION + \
                          $NACO_BINARY_AGE`

    AC_SUBST(NACO_VERSION)
    AC_SUBST(NACO_MAJOR_VERSION)
    AC_SUBST(NACO_MINOR_VERSION)
    AC_SUBST(NACO_MICRO_VERSION)
    AC_SUBST(NACO_INTERFACE_AGE)
    AC_SUBST(NACO_BINARY_VERSION)
    AC_SUBST(NACO_BINARY_AGE)

    AC_DEFINE_UNQUOTED(NACO_MAJOR_VERSION, $NACO_MAJOR_VERSION,
                       [NACO major version number])
    AC_DEFINE_UNQUOTED(NACO_MINOR_VERSION, $NACO_MINOR_VERSION,
                       [NACO minor version number])
    AC_DEFINE_UNQUOTED(NACO_MICRO_VERSION, $NACO_MICRO_VERSION,
                       [NACO micro version number])
    AC_DEFINE_UNQUOTED(NACO_INTERFACE_AGE, $NACO_INTERFACE_AGE,
                       [NACO interface age])
    AC_DEFINE_UNQUOTED(NACO_BINARY_VERSION, $NACO_BINARY_VERSION,
                       [NACO binary version number])
    AC_DEFINE_UNQUOTED(NACO_BINARY_AGE, $NACO_BINARY_AGE,
                       [NACO binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# NACO_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([NACO_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi
    apidocdir='${pipedocsdir}/html'

    if test -z "$configdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$privatelibdir"; then
       privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(apidocdir)
    AC_SUBST(configdir)
    AC_SUBST(privatelibdir)
    AC_SUBST(pipedocsdir)

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(NACO_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(NACO_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# NACO_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([NACO_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    NACO_INCLUDES='-I$(top_srcdir)/naco -I$(top_srcdir)/irplib -I$(top_srcdir)/hdrl'
    NACO_LDFLAGS='-L$(top_builddir)/naco'

    # No -L for IRPLIB which is statically linked
    all_includes='$(NACO_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(NACO_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    # Library aliases

    LIBNACO='$(top_builddir)/naco/libnaco.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Substitute the defined symbols

    AC_SUBST(NACO_INCLUDES)
    AC_SUBST(NACO_LDFLAGS)

    AC_SUBST(LIBNACO)
    AC_SUBST(LIBIRPLIB)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(NACO_INCLUDES) $(CPL_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(NACO_LDFLAGS) $(CPL_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])



# NACO_ENABLE_STRICT(strict=no)
#-----------------------------
AC_DEFUN([NACO_ENABLE_STRICT],
[
    AC_REQUIRE([AC_PROG_EGREP])
    AC_REQUIRE([AC_PROG_CC])

    AC_ARG_ENABLE(strict,
                  AS_HELP_STRING([--enable-strict],
                                 [compiles with extra, strict compiler options (may not work!) [default=$1]]),
                  naco_enable_strict=$enableval, naco_enable_strict=$1)

    AC_CACHE_CHECK([whether extra, strict compiler options should be used],
                   naco_cv_enable_strict,
                   naco_cv_enable_strict=$naco_enable_strict)


    if test x"$naco_cv_enable_strict" = xyes; then
    
        naco_enable_strict_std_set=no

        if test -n "$CFLAGS"; then
            echo $CFLAGS | $EGREP '(\-std=|-ansi)' >/dev/null 2>&1
            if test x"$?" = x0; then
                naco_enable_strict_std_set=yes
            fi
        fi
        
        if test x"$naco_enable_strict_std_set" = xno; then
                ESO_PROG_CC_FLAG([std=c99], [CFLAGS="$CFLAGS -std=c99"])
        fi
        
        ESO_PROG_CC_FLAG([pedantic], [CFLAGS="$CFLAGS -pedantic"])

        ESO_PROG_CC_FLAG([Werror=implicit-function-declaration],
                         [CFLAGS="$CFLAGS -Werror=implicit-function-declaration"])
        ESO_PROG_CC_FLAG([Werror=incompatible-pointer-types],
                         [CFLAGS="$CFLAGS -Werror=incompatible-pointer-types"])
        ESO_PROG_CC_FLAG([Werror=format=2], [CFLAGS="$CFLAGS -Werror=format=2"])
        ESO_PROG_CC_FLAG([Werror=format-nonliteral],
                         [CFLAGS="$CFLAGS -Werror=format-nonliteral"])
        ESO_PROG_CC_FLAG([Werror=format-security],
                         [CFLAGS=" $CFLAGS -Werror=format-security"])
        ESO_PROG_CC_FLAG([Werror=format-y2k],
                         [CFLAGS="$CFLAGS -Werror=format-y2k"])
        ESO_PROG_CC_FLAG([Werror=init-self],
                         [CFLAGS="$CFLAGS -Werror=init-self"])
        ESO_PROG_CC_FLAG([Werror=missing-declarations],
                         [CFLAGS="$CFLAGS -Werror=missing-declarations"])
        ESO_PROG_CC_FLAG([Werror=missing-format-attribute],
                         [CFLAGS="$CFLAGS -Werror=missing-format-attribute"])
        ESO_PROG_CC_FLAG([Werror=missing-noreturn],
                         [CFLAGS="$CFLAGS -Werror=missing-noreturn"])
        ESO_PROG_CC_FLAG([Werror=nested-externs],
                         [CFLAGS="$CFLAGS -Werror=nested-externs"])
        ESO_PROG_CC_FLAG([Werror=pointer-arith],
                         [CFLAGS="$CFLAGS -Werror=pointer-arith"])
        ESO_PROG_CC_FLAG([Werror=sign-compare],
                         [CFLAGS="$CFLAGS -Werror=sign-compare"])
        ESO_PROG_CC_FLAG([Werror=undef],
                         [CFLAGS="$CFLAGS -Werror=undef"])
        ESO_PROG_CC_FLAG([Werror=variadic-macros],
                         [CFLAGS="$CFLAGS -Werror=variadic-macros"])
        ESO_PROG_CC_FLAG([Werror=volatile-register-var],
                         [CFLAGS="$CFLAGS -Werror=volatile-register-var"])
        ESO_PROG_CC_FLAG([Werror=write-strings],
                         [CFLAGS="$CFLAGS -Werror=write-strings"])
        ESO_PROG_CC_FLAG([Werror=old-style-definition],
                         [CFLAGS="$CFLAGS -Werror=old-style-definition"])
        ESO_PROG_CC_FLAG([Werror=missing-prototypes],
                         [CFLAGS="$CFLAGS -Werror=missing-prototypes"])
        ESO_PROG_CC_FLAG([Werror=maybe-uninitialized],
                         [CFLAGS="$CFLAGS -Werror=maybe-uninitialized"])
        ESO_PROG_CC_FLAG([Werror=discarded-qualifiers],
                         [CFLAGS="$CFLAGS -Werror=discarded-qualifiers"])
        ESO_PROG_CC_FLAG([Werror=unused-but-set-variable],
                         [CFLAGS="$CFLAGS -Werror=unused-but-set-variable"])
        ESO_PROG_CC_FLAG([Werror=unused-parameter],
                         [CFLAGS="$CFLAGS -Werror=unused-parameter"])
        ESO_PROG_CC_FLAG([Werror=unused-variable],
                         [CFLAGS="$CFLAGS -Werror=unused-variable"])

        # Cannot make this an error due to include <unistd.h> on Mac OSX 10.8
        ESO_PROG_CC_FLAG([Wredundant-decls],
                         [CFLAGS="$CFLAGS -Wredundant-decls"])
        # Cannot make this an error w. fno-common on Mac OSX 10.9
        ESO_PROG_CC_FLAG([Wunused-function],
                         [CFLAGS="$CFLAGS -Wunused-function"])

        ESO_PROG_CC_FLAG([fno-common], [CFLAGS="$CFLAGS -fno-common"])
        ESO_PROG_CC_FLAG([fstrict-aliasing], [CFLAGS="$CFLAGS -fstrict-aliasing"])
        
    fi
])
